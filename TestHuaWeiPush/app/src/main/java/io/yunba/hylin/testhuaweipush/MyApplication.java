package io.yunba.hylin.testhuaweipush;

import android.app.Application;

import io.yunba.hylin.testhuaweipush.activity.MainActivity;


/**
 * Author：林恒宜 on 16-8-6 14:31
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyApplication extends Application {

    private static MainActivity mMainActivity;
    private static MyApplication mInstance;

    public void setmMainActivity(MainActivity mMainActivity) {
        MyApplication.mMainActivity = mMainActivity;
    }

    public MainActivity getmMainActivity() {
        return mMainActivity;
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        exit();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc(); // 通知回收垃圾
    }

    private void exit() {
        if (mMainActivity != null) {
            mMainActivity.finish();
        }
        System.exit(0);
    }
}
