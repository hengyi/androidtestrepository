package io.yunba.hylin.testhuaweipush.constant;

/**
 * Author：林恒宜 on 16-8-6 18:18
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class Constant {
    public static final String TAG = "debug_tag";

    public static final String REQUEST_TOKEN_SUCCESS = "request_token_success";
    public static final String SET_TOPIC_SUCCESS = "set_topic_success";
    public static final String RECEIVE_PUSH_MESSAGE_SUCCESS = "receive_push_message_success";




    public static final int REQUEST_TOKEN_SUCCESS_CODE = 0; // 注册成功
    public static final int SET_TOPIC_SUCCESS_CODE     = 1; // 设置频道成功
    public static final int RECEIVE_PUSH_MESSAGE_SUCCESS_CODE = 2;// 收到推送消息成功
}
