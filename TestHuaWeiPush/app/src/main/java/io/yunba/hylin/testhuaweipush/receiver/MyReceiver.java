package io.yunba.hylin.testhuaweipush.receiver;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.huawei.android.pushagent.api.PushEventReceiver;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.yunba.hylin.testhuaweipush.MyApplication;
import io.yunba.hylin.testhuaweipush.activity.MainActivity;
import io.yunba.hylin.testhuaweipush.constant.Constant;


/**
 * Author：林恒宜 on 16-8-4 10:44
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyReceiver extends PushEventReceiver {

    /**
     * 描述：pushToken申请成功后,会自动回调该方法,应用可以通过该接口中获取token。本接口必须被实现
     *
     * @param context 程序运行上下文
     * @param token   应用唯一标识Token
     * @param extras  扩展信息
     */
    @Override
    public void onToken(Context context, String token, Bundle extras) {
        super.onToken(context, token, extras);
        Log.i(Constant.TAG, Constant.REQUEST_TOKEN_SUCCESS + "Token是：" + token);
        showPushMessage(Constant.REQUEST_TOKEN_SUCCESS_CODE, "[" + getCurrentTime() + "] request token success," +
                "token = " + token + "\n\n");
    }
    /**
     * 描述：推送消息下来时会自动回调onPushMsg方法实现应用透传消息处理。本接口必须被实现
     *
     * @param context  程序运行的上下文
     * @param msgBytes 透传消息字节数组
     * @param extras   扩展信息暂时不启用
     * @return
     */
    @Override
    public boolean onPushMsg(Context context, byte[] msgBytes, Bundle extras) {
        Log.i(Constant.TAG,"收到一条消息：" + msgBytes.toString());
        return super.onPushMsg(context, msgBytes, extras);
    }

    /**
     * 实现业务事件。该方法会在设置标签、LBS信息之后、点击打开通知栏消息、点击通知栏上的按钮之后被调用。
     * 由业务决定是否调用该函数。
     *
     * @param context 程序运行的上下文
     * @param event   事件类型 event为枚举类型,事件定义如下
     *                Event定义事件如下:
     *                public static enum Event
     *                {
     *                NOTIFICATION_OPENED，      //通知栏中的通知被点击打开
     *                NOTIFICATION_CLICK_BTN,   //通知栏中通知上的按钮被点击
     *                PLUGINRSP,                //标签上报回应
     *                }
     * @param extras  扩展信息extras为携带的参数,应用可根据不同事件获取不同参数作处理。可参看SDK文档来处理
     */
    @Override
    public void onEvent(Context context, Event event, Bundle extras) {

        //标签上报回应
        if (Event.PLUGINRSP.equals(event)) {
            /*
             * 该类型下extras 参数
             * （1）BOUND_KEY.PLUGINREPORTTYPE 上报类型,1为LBS(当前暂不支持),2为标签。
             * （2）BOUND_KEY.PLUGINREPORTRESULT 上报结果,成功则为true,默认为false。
             */
            final int TYPE_LBS = 1; // 当前暂不支持
            final int TYPE_TAG = 2; // 标签
            int type = extras.getInt(BOUND_KEY.PLUGINREPORTTYPE, -1);
            boolean isSuccess = extras.getBoolean(BOUND_KEY.PLUGINREPORTRESULT, false);
            StringBuilder message = new StringBuilder();
            if (type == TYPE_LBS) {
                message.append("LBS Result is :");
            } else if (type == TYPE_TAG) {
                message.append("LAG Result is :");
            }
            showPushMessage(Constant.SET_TOPIC_SUCCESS_CODE,"[" + getCurrentTime() +"]" + message.toString() + isSuccess + "\n\n");

        }
        //通知栏中通知上的按钮被点击
        else if (Event.NOTIFICATION_CLICK_BTN.equals(event) || Event.NOTIFICATION_OPENED.equals(event)) {
            Log.i(Constant.TAG, Constant.RECEIVE_PUSH_MESSAGE_SUCCESS + "notification_click_btn");
            /*
             * 该类型下extras 参数
             * （1）BOUND_KEY.pushMsgKey   匹配该字符串以决定点击按钮后处理何种事件
             * （2）BOUND_KEY.pushNotifyId 通知栏id,点击通知栏上的按钮后,业务可根据需要清除掉该通知栏或保留;默认为保留。
             */
            int notifyId = extras.getInt(BOUND_KEY.pushNotifyId, -1);
            if (notifyId != -1) {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(notifyId);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("收到的消息是：").append(extras.getString(BOUND_KEY.pushMsgKey, ""));
            showPushMessage(Constant.RECEIVE_PUSH_MESSAGE_SUCCESS_CODE,getCurrentTime() + sb.toString() +"\n\n");
        }
        //通知栏中的通知被点击打开
//        else if (Event.NOTIFICATION_OPENED.equals(event)) {
//            Log.i(Constant.TAG,Constant.RECEIVE_PUSH_MESSAGE_SUCCESS + "notification_opened"); // 不一定会回调该方法
//            /*
//             * 该类型下extras 参数
//             * （1）BOUND_KEY.pushMsgKey 附加信息,由业务自行处理。 需要在华为开发者联盟上推送消息时添加自定义键值对才回调。
//             */
//        }
        super.onEvent(context, event, extras);
    }

    /**
     * 显示推送消息,在该方法里主要就是实现发送消息，让MainActivity去处理消息
     *
     * @param type    消息的类型
     * @param message 消息的内容
     */
    private void showPushMessage(int type, String message) {
        MainActivity mMainActivity = MyApplication.getInstance().getmMainActivity();
        if (mMainActivity != null) {
            Handler handler = mMainActivity.getmHanlder();
            if (handler != null) {
                Message msg = handler.obtainMessage();
                msg.what = type;
                msg.obj = message;
                handler.sendMessage(msg);
            }
        }
    }

    /**
     * 获取当前时间并格式化
     * @return 格式化的时间
     */
    private String getCurrentTime() {
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        String currentTime = format.format(new Date());
        return currentTime;
    }

}
