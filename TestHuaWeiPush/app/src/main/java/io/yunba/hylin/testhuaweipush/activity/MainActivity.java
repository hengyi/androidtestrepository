package io.yunba.hylin.testhuaweipush.activity;

import android.app.ActivityManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huawei.android.pushagent.PushManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.yunba.hylin.testhuaweipush.MyApplication;
import io.yunba.hylin.testhuaweipush.R;
import io.yunba.hylin.testhuaweipush.constant.Constant;


public class MainActivity extends AppCompatActivity {

    private TextView tvShowMessage;

    // 在异步消息处理器中根据不同消息类型处理消息
    private Handler mHanlder = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String message = (String) msg.obj;
            StringBuilder sb = new StringBuilder();
            String currentText = "";
            switch (msg.what) {
                case Constant.REQUEST_TOKEN_SUCCESS_CODE:
                    Toast.makeText(MainActivity.this, Constant.REQUEST_TOKEN_SUCCESS, Toast.LENGTH_SHORT).show();
                    currentText = tvShowMessage.getText().toString().trim();
                    sb.append(currentText).append("\n\n").append(message);
                    tvShowMessage.setText(sb.toString());
                    break;
                case Constant.SET_TOPIC_SUCCESS_CODE:
                    Toast.makeText(MainActivity.this, Constant.SET_TOPIC_SUCCESS, Toast.LENGTH_SHORT).show();
                    currentText = tvShowMessage.getText().toString().trim();
                    sb = new StringBuilder();
                    sb.append(currentText).append("\n\n").append(message);
                    tvShowMessage.setText(sb.toString());
                    break;
                case Constant.RECEIVE_PUSH_MESSAGE_SUCCESS_CODE:
                    Toast.makeText(MainActivity.this, Constant.RECEIVE_PUSH_MESSAGE_SUCCESS, Toast.LENGTH_SHORT).show();
                    currentText = tvShowMessage.getText().toString().trim();
                    sb = new StringBuilder();
                    sb.append(currentText).append("\n\n").append(message);
                    tvShowMessage.setText(sb.toString());
                    break;
            }
        }
    };

    public Handler getmHanlder() {
        return mHanlder;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.getInstance().setmMainActivity(this);
        tvShowMessage = (TextView) findViewById(R.id.tvShowMessage);

        findViewById(R.id.btnGetToken).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"requestToken()执行啦",Toast.LENGTH_SHORT).show();
                PushManager.requestToken(MainActivity.this); // 启动就联网申请许可Token
            }
        });

        findViewById(R.id.btnSetTopic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout layout = new LinearLayout(getApplicationContext());
                layout.setOrientation(LinearLayout.VERTICAL);
                final EditText etName = new EditText(getApplicationContext());
                final EditText etTopic = new EditText(getApplicationContext());
                etName.setHint("请填写键");
                etTopic.setHint("请填写值");
                etName.setHintTextColor(Color.CYAN);
                etTopic.setHintTextColor(Color.CYAN);
                etName.setTextColor(Color.BLACK);
                etTopic.setTextColor(Color.BLACK);
                layout.addView(etName, 0);
                layout.addView(etTopic, 1);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请填写要设置频道信息")
                        .setView(layout)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = etName.getText().toString().trim();
                                String topic = etTopic.getText().toString().trim();
                                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(topic)) {
                                    Toast.makeText(MainActivity.this, "不能为空，请填写内容", Toast.LENGTH_SHORT).show();
                                } else {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("当期输入的名字是：" + name + "当前输入的频道是：" + topic);
                                    Map<String, String> settingInfo = new HashMap<>();
                                    settingInfo.put(name, topic);
                                    PushManager.setTags(getApplicationContext(), settingInfo);
                                    SharedPreferences config = getSharedPreferences("config", MODE_PRIVATE);
                                    config.edit().putString(name, topic).commit();
                                    Toast.makeText(getApplicationContext(), sb.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });

        findViewById(R.id.btnGetTopic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> topics = PushManager.getTags(getApplicationContext());
                if (topics != null && (!topics.isEmpty())) {
                    for (Map.Entry<String, String> entry : topics.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        showCurrentSubscribeTopics(key, value);
                    }
                }

            }
        });

        findViewById(R.id.btnDeleteTopic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText etDeleteTopic = new EditText(getApplicationContext());
                etDeleteTopic.setHint("请输入要删除的频道的名称");
                etDeleteTopic.setHintTextColor(Color.CYAN);
                etDeleteTopic.setTextColor(Color.BLACK);
                new AlertDialog.Builder(getApplicationContext()).setTitle("请输入要删除的频道的名称")
                        .setView(etDeleteTopic)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String topic = etDeleteTopic.getText().toString().trim();
                                if (TextUtils.isEmpty(topic)) {
                                    Toast.makeText(MainActivity.this, "输入不能为空", Toast.LENGTH_SHORT).show();
                                } else {
                                    List<String> topics = new ArrayList<String>();
                                    topics.add(topic);
                                    PushManager.deleteTags(getApplicationContext(), topics);
                                }
                            }
                        })
                        .setPositiveButton("取消", null).show();
            }
        });
    }

    /**
     * 判断是否需要申请Token
     * @return 需要则返回true否则返回false
     */
    private boolean shouldRequestToken() {
        ActivityManager mActivityService = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = mActivityService.getRunningAppProcesses();
        String mainProName = getPackageName();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : runningAppProcesses) {
            if (myPid == info.pid && mainProName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

    private void showCurrentSubscribeTopics(String key, String value) {
        String currentText = tvShowMessage.getText().toString().trim();
        StringBuilder sb = new StringBuilder();
        sb.append(currentText).append("\n\n").append("key = ").append(key).append("value = ").append(value).append("\n\n");
        tvShowMessage.setText(sb.toString());
    }
}
