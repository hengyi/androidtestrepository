package io.yunba.testyunbaxiaomi.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import io.yunba.android.manager.YunBaManager;
import io.yunba.testyunbaxiaomi.bean.Constant;
import io.yunba.testyunbaxiaomi.utils.MyUtils;

/**
 * Author：林恒宜 on 16-8-2 10:40
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description : 处理接受到的广播信息 这个广播接收器工作于后台状态下，主要用作弹出通知栏
 */
public class MyYunBaReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //判断接收到的是不是云吧推送的信息
        if (YunBaManager.MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
            String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
            String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
            StringBuilder sb = new StringBuilder();
            sb.append("收到的消息是频道是：").append(topic).append(";消息的内容是").append(msg);
            Log.i(Constant.TAG, sb.toString());
            MyUtils.showNotification(context, topic, msg);

        } else if (YunBaManager.PRESENCE_RECEIVED_ACTION.equals(intent.getAction())) {
            String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
            String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
            MyUtils.showNotification(context, topic, msg);
        }
    }
}
