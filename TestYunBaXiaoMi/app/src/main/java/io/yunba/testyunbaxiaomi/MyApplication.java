package io.yunba.testyunbaxiaomi;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.xiaomi.mipush.sdk.MiPushClient;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

import java.util.List;

import io.yunba.android.manager.YunBaManager;
import io.yunba.testyunbaxiaomi.activity.MainActivity;
import io.yunba.testyunbaxiaomi.bean.Constant;
import io.yunba.testyunbaxiaomi.utils.MyUtils;
import io.yunba.testyunbaxiaomi.utils.SharepreferenceHelper;

/**
 * Author：林恒宜 on 16-8-2 08:44
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description : 在Application中启动云吧服务
 */
public class MyApplication extends Application {

    private static MainActivity mActivity = null;
    private static MyHanlder mHanlder = null ;

    public static void setmActivity(MainActivity mActivity) {
        MyApplication.mActivity = mActivity;
    }

    public static MyHanlder getMHanlder () {
        return mHanlder;
    }

    public static class MyHanlder extends Handler {
        private Context context;

        public MyHanlder(Context context) {
            this.context = context;
        }
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case Constant.REGISTER_PUSH_SUCCESS_CODE :
                    MyUtils.showToast(context,"注册成功");
                    break;
                case Constant.SET_ALIAS_SUCCESS_CODE :
                    MyUtils.showToast(context,"设置别名成功");
                    break;
                case Constant.SET_USERACCOUNT_SUCCESS_CODE :
                    MyUtils.showToast(context,"设置用户账户成功");
                    break;
                case Constant.SUBSCRIBE_TOPIC_SUCCESS_CODE :
                    MyUtils.showToast(context,"订阅主题成功");
                    break;
                case Constant.UNSET_ALIAS_SUCCESS_CODE :
                    MyUtils.showToast(context,"取消别名成功");
                    break;
                case Constant.UNSET_USERACCOUNT_SUCCESS_CODE:
                    MyUtils.showToast(context,"取消用户账户成功");
                    break;
                case Constant.UNSUBSCRIBE_SUCCESS_CODE:
                    MyUtils.showToast(context,"取消订阅主题成功");
                    break;
                case Constant.PAUSH_PUSH_SUCCESS_CODE:
                    MyUtils.showToast(context,"暂停接收消息成功");
                    break;
                case Constant.RESUME_PUSH_SUCCESS_CODE:
                    MyUtils.showToast(context,"恢复接收消息成功");
                    break;
                case Constant.SET_TIME_SUCCESS_CODE:
                    //设置时间
                    break;
                case Constant.RECEIVE_MESSAGE_SUCCESS_CODE:
                    if (mActivity != null) {
                        mActivity.refreshLogView();
                    }
                    MyUtils.showToast(context,"收到消息");
                    break;
                case Constant.RECEIVE_PASS_THROUGH_MESSAGE_SUCCESS_CODE:
                    if (mActivity != null) {
                        mActivity.refreshLogView();
                    }
                    MyUtils.showToast(context,"收到透传消息");
                    break;
                case Constant.RECEIVE_NOTIFICATION_MESSAGE_SUCCESS_CODE:
                    if (mActivity != null) {
                        mActivity.refreshLogView();
                    }
                    MyUtils.showToast(context,"收到通知栏消息");
                    break;
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String manufacturer = Build.MANUFACTURER;
        if (manufacturer.equals("Xiaomi")) {
            /*如果是小米手机就注册小米的服务*/
            if(shouldInit()) {
                MiPushClient.registerPush(this, Constant.APP_ID, Constant.APP_KEY);
            }

        } else {
            //如果不是小米手机就注册云吧的服务
            YunBaManager.start(getApplicationContext());  //初始化云吧SDK
            SharepreferenceHelper.setBoolean(getApplicationContext(),Constant.NETSTATE,false);
            YunBaManager.subscribe(getApplicationContext(), new String[]{"1","2","3","4"}, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken iMqttToken) {
                    Log.i(Constant.TAG, "subscribe success");
                    SharepreferenceHelper.setBoolean(getApplicationContext(),Constant.NETSTATE,true);
                }

                @Override
                public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                    Log.i(Constant.TAG, "subscribe unsuccess");
                    SharepreferenceHelper.setBoolean(getApplicationContext(),Constant.NETSTATE,false);
                }
            });
        }
    }

    private boolean shouldInit() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
