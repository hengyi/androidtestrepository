package io.yunba.testyunbaxiaomi.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.Random;

import io.yunba.android.manager.YunBaManager;
import io.yunba.testyunbaxiaomi.R;
import io.yunba.testyunbaxiaomi.activity.MainActivity;

/**
 * Author：林恒宜 on 16-8-2 10:53
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyUtils {
    public static boolean showNotification(Context context, String topic, String msg) {
        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = new long[]{500,500,500};

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context) // 新建通知
                .setSmallIcon(R.mipmap.ic_launcher) // 小图标
                .setContentTitle(topic)             // 标题
                .setContentText(msg)                // 文本详细内容
                .setSound(alarmUri)                 // 设置声音
                .setVibrate(pattern)                // 设置模式
                .setAutoCancel(true);               // 设置可以自动取消

        Intent intent = new Intent(context, MainActivity.class);
        if (!TextUtils.isEmpty(topic)) {
            intent.putExtra(YunBaManager.MQTT_TOPIC,topic);
        }
        if (!TextUtils.isEmpty(msg)) {
            intent.putExtra(YunBaManager.MQTT_MSG,msg);
        }

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(new Random().nextInt(), builder.build());
        return true;
    }

    public static void showToast(final Context context, final String content){
        if (TextUtils.isEmpty(content)) {
            return ;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }).start();
    }

    /**
     * 获取AppKEY
     * @param context
     * @return
     */
    public static String getAPPKey(Context context) {
        Bundle metadata = null;
        String appKey = null;
        try {
            ApplicationInfo info = context.getPackageManager()
                    .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            if (info != null) {
                metadata = info.metaData;
            }
            if (metadata != null) {
                appKey = metadata.getString("YUNBA_APPKAY");
                if (appKey == null || appKey.length() != 24) {
                    appKey = "ERROR";
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appKey;
    }

//    public static void showMessage (Context context,final String topic, final String msg) {
//        context.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                StringBuilder sb = new StringBuilder();
//                sb.append("topic = " + topic);
//                sb.append("message = " + msg);
//                tvShowMsg.setText(sb.toString());
//            }
//        });
//    }
}
