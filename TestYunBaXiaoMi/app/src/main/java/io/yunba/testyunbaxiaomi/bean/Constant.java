package io.yunba.testyunbaxiaomi.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Author：林恒宜 on 16-8-2 14:38
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class Constant {
    public static final String TAG = "debug_tag";
    public static final String NETSTATE = "net_state";
    public static List<MyMessage> msgList = new ArrayList<>();

    public static final String APP_ID = "2882303761517498762";
    public static final String APP_KEY = "5641749854762";

    public static String mRegId ;
    public static String mAlias ;
    public static String mUserAccount;
    public static String mSubscribeTopic;
    public static String mStartTime ;
    public static String mEndTime ;

    public static final String REGISTER_SUCCESS = "register push service success";
    public static final String SET_ALIAS_SUCCESS = "set alias success";
    public static final String SET_USER_ACCOUNT_SUCCESS = "set user account success";
    public static final String SUBSCRIBE_TOPIC_SUCCESS = "subscribe topic success";
    public static final String SET_ACCEPT_TIME_SUCCESS = "set accept time success";
    public static final String UNSET_ACCOUNT_SUCCESS = "unset account success";
    public static final String UNSET_ALIAS_SUCCESS = "unset alias success";
    public static final String UN_SUBSRCIBE_TOPIC_SUCCESS = "unsubscribe topic success";
    public static final String RECEIVE_THROUGH_MESSAGE_FROM_SERVER = "receive through message from server success";
    public static final String RECEIVE_MESSAGE_SUCCESS = "receive message success";
    public static final String RECEIVE_NOTIFICATION_MESSAGE_SUCCESS = "receive notification message success";
    public static final String NOTIFIVATION_MESSAGE_ONCLICK_SUCCESS = "notification message onclick success";

    public static final int REGISTER_PUSH_SUCCESS_CODE      = 0;
    public static final int SET_ALIAS_SUCCESS_CODE          = 1;
    public static final int SET_USERACCOUNT_SUCCESS_CODE    = 2;
    public static final int SUBSCRIBE_TOPIC_SUCCESS_CODE    = 3;
    public static final int UNSET_ALIAS_SUCCESS_CODE        = 4;
    public static final int UNSET_USERACCOUNT_SUCCESS_CODE  = 5;
    public static final int UNSUBSCRIBE_SUCCESS_CODE        = 6;
    public static final int PAUSH_PUSH_SUCCESS_CODE         = 7;
    public static final int RESUME_PUSH_SUCCESS_CODE        = 8;
    public static final int SET_TIME_SUCCESS_CODE           = 9;
    public static final int RECEIVE_NOTIFICATION_MESSAGE_SUCCESS_CODE = 10;
    public static final int RECEIVE_PASS_THROUGH_MESSAGE_SUCCESS_CODE = 11;
    public static final int RECEIVE_MESSAGE_SUCCESS_CODE    = 12;
}
