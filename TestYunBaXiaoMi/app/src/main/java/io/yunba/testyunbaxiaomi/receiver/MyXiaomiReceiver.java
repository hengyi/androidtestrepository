package io.yunba.testyunbaxiaomi.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

import java.util.List;

import io.yunba.testyunbaxiaomi.MyApplication;
import io.yunba.testyunbaxiaomi.activity.MainActivity;
import io.yunba.testyunbaxiaomi.bean.Constant;
import io.yunba.testyunbaxiaomi.bean.MyMessage;

/**
 * Author：林恒宜 on 16-8-8 13:20
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyXiaomiReceiver extends PushMessageReceiver {
    public MyXiaomiReceiver() {
    }
    /**
     * 处理透传信息
     * 把消息取出后封装到常量类的消息列表中去
     * 由MainActivity来处理刷新消息列表
     * @param context 程序运行的上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceivePassThroughMessage(context, miPushMessage);
        Log.i(Constant.TAG,Constant.RECEIVE_THROUGH_MESSAGE_FROM_SERVER);
        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic(); // 消息的主题
        Log.i(Constant.TAG, "消息内容：" + messageContent + "消息主题是：" + messageTopic);
        MyMessage myMessage = new MyMessage(messageTopic,messageContent);
        Constant.msgList.add(0,myMessage);
        Message msg = Message.obtain();
        msg.what =Constant.RECEIVE_PASS_THROUGH_MESSAGE_SUCCESS_CODE;
        MyApplication.getMHanlder().sendMessage(msg);
    }

    /**
     * 处理收到的通知栏消息点击事件
     * @param context 程序运行的上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageClicked(context, miPushMessage);
        Log.i(Constant.TAG,Constant.NOTIFIVATION_MESSAGE_ONCLICK_SUCCESS);
        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic();     // 消息频道

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("msgContent",messageContent);
        intent.putExtra("msgTopic",messageTopic);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 处理点击通知栏消息
     * @param context 程序运行的上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageArrived(context, miPushMessage);
        Log.i(Constant.TAG,Constant.RECEIVE_NOTIFICATION_MESSAGE_SUCCESS);
        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic();
        Log.i(Constant.TAG,"onNotificationMessageArrived方法收到的消息内容是：" + messageContent);
        MyMessage myMessage = new MyMessage(messageTopic,messageContent);
        Constant.msgList.add(0,myMessage);
        Message msg = Message.obtain();
        msg.what = Constant.RECEIVE_NOTIFICATION_MESSAGE_SUCCESS_CODE;
        MyApplication.getMHanlder().sendMessage(msg);
    }

    /**
     * 收到消息就会执行此段代码
     * 可以空实现
     * @param context 程序运行上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onReceiveMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceiveMessage(context, miPushMessage);

        Log.i(Constant.TAG,Constant.RECEIVE_MESSAGE_SUCCESS);
    }

    /**
     * 处理收到注册命令返回的消息
     * @param context 程序运行的上下文
     * @param miPushCommandMessage 返回的消息对象
     */
    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onReceiveRegisterResult(context, miPushCommandMessage);
        List<String> arguments = miPushCommandMessage.getCommandArguments();
        String cmdArg1 = (arguments != null && arguments.size() > 0) ? arguments.get(0) : null ;
        String cmdArg2 = (arguments != null && arguments.size() > 1) ? arguments.get(1) : null;
        if (miPushCommandMessage.getCommand().equals(MiPushClient.COMMAND_REGISTER)) {
            if (miPushCommandMessage.getResultCode() == ErrorCode.SUCCESS) {
                Constant.mRegId = cmdArg1 ;
            }
        }
    }

    /**
     * 根据返回的命令的类型相应的处理各个信息
     * @param context 程序运行的上下文
     * @param miPushCommandMessage 返回的消息，该消息封装在MiPushCommandMessage类中
     */
    @Override
    public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onCommandResult(context, miPushCommandMessage);
        long resultCode = miPushCommandMessage.getResultCode();
        List<String> arguments = miPushCommandMessage.getCommandArguments();
        String arg1 = (arguments != null && arguments.size() > 0) ? arguments.get(0) : null;
        String arg2 = (arguments != null && arguments.size() > 1) ? arguments.get(1) : null; // 第二个参数有可能没有
        if (resultCode == ErrorCode.SUCCESS) {

            if (MiPushClient.COMMAND_REGISTER.equals(miPushCommandMessage.getCommand())) {
                // 处理注册成功的返回码
                Constant.mRegId = arg1;
                Message msg = MyApplication.getMHanlder().obtainMessage();
                msg.what = Constant.REGISTER_PUSH_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);

                Log.i(Constant.TAG,Constant.REGISTER_SUCCESS);

            } else if (MiPushClient.COMMAND_SET_ALIAS.equals(miPushCommandMessage.getCommand())) {
                // 处理设置别名的返回码
                Constant.mAlias = arg1;

                Message msg = Message.obtain();
                msg.what = Constant.SET_ALIAS_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG, Constant.SET_ALIAS_SUCCESS);

            } else if (MiPushClient.COMMAND_SET_ACCOUNT.equals(miPushCommandMessage.getCommand())) {
                // 处理设置账户的返回码
                Constant.mUserAccount = arg1;

                Message msg = Message.obtain();
                msg.what = Constant.SET_USERACCOUNT_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG,Constant.SET_USER_ACCOUNT_SUCCESS);

            } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(miPushCommandMessage.getCommand())) {
                // 处理设置接收时间段的返回码
                Constant.mStartTime = arg1;
                Constant.mEndTime = arg2;

                Message msg = Message.obtain();
                msg.what = Constant.SET_TIME_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG,Constant.SET_ACCEPT_TIME_SUCCESS);

            } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(miPushCommandMessage.getCommand())) {
                // 处理订阅频道的返回码
                Constant.mSubscribeTopic = arg1;

                Message msg = Message.obtain();
                msg.what = Constant.SUBSCRIBE_TOPIC_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG, Constant.SUBSCRIBE_TOPIC_SUCCESS);

            } else if (MiPushClient.COMMAND_UNSET_ACCOUNT.equals(miPushCommandMessage.getCommand())) {
                // 处理取消设置账户的返回码
                Constant.mUserAccount = arg1;

                Message msg = Message.obtain();
                msg.what = Constant.UNSET_USERACCOUNT_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG, Constant.UNSET_ACCOUNT_SUCCESS);

            } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(miPushCommandMessage.getCommand())) {
                // 处理取消设置别名的返回码
                Constant.mAlias = arg1;

                Message msg = Message.obtain();
                msg.what = Constant.UNSET_ALIAS_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG, Constant.UNSET_ALIAS_SUCCESS);

            } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(miPushCommandMessage.getCommand())) {
                // 处理取消订阅频道的返回码
                Constant.mSubscribeTopic = arg1;

                Message msg = Message.obtain();
                msg.what = Constant.UNSUBSCRIBE_SUCCESS_CODE;
                MyApplication.getMHanlder().sendMessage(msg);
                Log.i(Constant.TAG, Constant.UN_SUBSRCIBE_TOPIC_SUCCESS);
            }
        }
    }
}
