package io.yunba.testyunbaxiaomi.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xiaomi.mipush.sdk.MiPushClient;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

import java.util.ArrayList;
import java.util.List;

import io.yunba.android.manager.YunBaManager;
import io.yunba.testyunbaxiaomi.MyApplication;
import io.yunba.testyunbaxiaomi.R;
import io.yunba.testyunbaxiaomi.bean.Constant;
import io.yunba.testyunbaxiaomi.bean.MyMessage;
import io.yunba.testyunbaxiaomi.utils.MyUtils;
import io.yunba.testyunbaxiaomi.utils.SharepreferenceHelper;

public class MainActivity extends AppCompatActivity {

    private EditText etTopic;
    private EditText etMsg;
    private EditText etSubscribeNum;
    private TextView tvShowMsg;

    private MessageReceiver mMessageReceiver;
    private EditText etAlias;
    private boolean isConnected;
    private List<String> topicList=new ArrayList<>();
    private EditText etXiaomiTopic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.setmActivity(this);
//        String model = Build.MODEL;
//        String manufacturer = Build.MANUFACTURER;
//        Log.i("xinghao", model);
//        Log.i("xiaomi", manufacturer);
        isConnected= SharepreferenceHelper.getBoolean(getApplicationContext(), Constant.NETSTATE, false);
        initUI();
        registerReciver();
    }

    private void initUI() {
        if (isConnected) {
            getSupportActionBar().setTitle("YUNBA->CONNECTED");
        } else {
            getSupportActionBar().setTitle("YUNBA->UNCONNECTED");
        }
        //声明文本框
        tvShowMsg=(TextView) findViewById(R.id.tvShowMsg);
        //声明按钮
        Button btnSelectTopic=(Button) findViewById(R.id.btnSelectTopic);
        Button btnSendMsg=(Button) findViewById(R.id.btnSendMsg);
        Button btnSetAlias=(Button) findViewById(R.id.btnSetAlias);
        Button btnSubscribe=(Button) findViewById(R.id.btnSubscribe);
        Button btnUnGetAlias=(Button) findViewById(R.id.btnUnGetAlias);
        Button btnUnSubscribe=(Button) findViewById(R.id.btnUnSubscribe);
        Button btnXiaomiSubscribeTopic = (Button) findViewById(R.id.btnXiaomiSubscribeTopic);
        //声明编辑框
        etTopic=(EditText) findViewById(R.id.etTopic);
        etMsg=(EditText) findViewById(R.id.etMsg);
        etSubscribeNum=(EditText) findViewById(R.id.etSubscribeNum);
        etAlias=(EditText) findViewById(R.id.etAlias);
        etXiaomiTopic = (EditText) findViewById(R.id.etXiaomiTopic);

        //设置按钮的监听器
        MyOnclickLinstener listener=new MyOnclickLinstener();

        if (btnSelectTopic != null) {
            btnSelectTopic.setOnClickListener(listener);
        }
        if (btnSendMsg != null) {
            btnSendMsg.setOnClickListener(listener);
        }
        if (btnSetAlias != null) {
            btnSetAlias.setOnClickListener(listener);
        }
        if (btnSubscribe != null) {
            btnSubscribe.setOnClickListener(listener);
        }
        if (btnUnGetAlias != null) {
            btnUnGetAlias.setOnClickListener(listener);
        }
        assert btnUnSubscribe != null; // 断言
        btnUnSubscribe.setOnClickListener(listener);

        if (btnXiaomiSubscribeTopic != null) {
            btnXiaomiSubscribeTopic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String xiaomiTopic = etXiaomiTopic.getText().toString().trim();
                    if (TextUtils.isEmpty(xiaomiTopic)) {
                        Toast.makeText(MainActivity.this,"小米频道不能为空，请重新输入",Toast.LENGTH_SHORT).show();
                    } else {
                        MiPushClient.subscribe(getApplicationContext(), xiaomiTopic, null);
                    }
                }
            });
        }
    }

    class MyOnclickLinstener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnSelectTopic:   //选择频道的对话框
                    selectTopic(getApplicationContext());          // 选择频道
                    break;
                case R.id.btnSendMsg:       // 发送消息
                    String msg=etMsg.getText().toString().trim();
                    String topic=etTopic.getText().toString().trim();
                    if (TextUtils.isEmpty(msg)) {
                        MyUtils.showToast(MainActivity.this, "消息不能为空");
                    } else if (TextUtils.isEmpty(topic)) {
                        MyUtils.showToast(MainActivity.this, "频道不能为空");
                    } else {
                        String currentContent=tvShowMsg.getText().toString().trim();
                        StringBuilder sb=new StringBuilder();
                        String result="Publishing Message:Topisc = " + topic + "Message = " + msg;
                        sb.append(currentContent).append("\n\n").append(result).append("\n\n");
                        showMessage(sb.toString());
                        sendMsg(MainActivity.this, topic, msg); // 发送消息的方法
                    }
                    break;
                case R.id.btnSetAlias:      // 设置别名
                    String alias=etAlias.getText().toString().trim();
                    if (TextUtils.isEmpty(alias)) {
                        Toast.makeText(MainActivity.this, "别名不能为空", Toast.LENGTH_SHORT).show();
                    } else {
                        setAlias(getApplicationContext(), alias);     // 设置别名方法
                    }
                    break;
                case R.id.btnSubscribe: // 订阅频道
                    String num=etSubscribeNum.getText().toString().trim();
                    if (TextUtils.isEmpty(num)) {
                        MyUtils.showToast(getApplicationContext(), "订阅频道号不能能为空");
                    } else {
                        subscribeTopic(MainActivity.this, num); //订阅频道的方法
                    }
                    break;
                case R.id.btnUnGetAlias: // 获取别名
                    getALias(getApplicationContext());
                    break;
                case R.id.btnUnSubscribe: // 取消订阅:
                    String result=etSubscribeNum.getText().toString().trim();
                    if (TextUtils.isEmpty(result)) {
                        Toast.makeText(MainActivity.this, "频道号不能为空", Toast.LENGTH_SHORT).show();
                    } else {
                        unSubsribe(getApplicationContext(), result);
                    }
                    break;
            }
        }
    }

    /**
     * 弹出选择发送频道号的对话框
     */
    private void selectTopic(Context context) {
        if (topicList.size() == 0) {
            return;
        }
        AlertDialog.Builder dialog=new AlertDialog.Builder(context).setTitle("请选择频道")
                .setAdapter(new ArrayAdapter<String>(context, R.layout.dialog_listview, topicList), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        etTopic.setText(topicList.get(which));
                        dialog.dismiss();
                    }
                });
        dialog.show();

    }

    /**
     * 发送消息
     *
     * @param context 运行上下文
     * @param topic   要发送到的频道号
     * @param msg     待发送的消息
     */
    private void sendMsg(Context context, final String topic, final String msg) {
        YunBaManager.publish(context, topic, msg, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                String content=tvShowMsg.getText().toString().trim();
                StringBuilder sb=new StringBuilder();
                sb.append(content).append("Send Message To topic:").append(topic).append("Success")
                        .append("Message").append(msg).append("\n\n");
                showMessage(sb.toString());
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                String content=tvShowMsg.getText().toString().trim();
                StringBuilder sb=new StringBuilder();
                sb.append(content).append("Send Message To topic:").append(topic).append("Faile");
                showMessage(sb.toString());
            }
        });
    }

    /**
     * 设置别名
     *
     * @param alias 别名
     */
    private void setAlias(final Context context, String alias) {
        YunBaManager.setAlias(context, alias, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                MyUtils.showToast(context, "设置别名成功");
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                MyUtils.showToast(context, "设置别名失败");
            }
        });
    }

    /**
     * 订阅频道号
     *
     * @param context 上下文环境
     * @param num     待订阅的频道号
     */
    private void subscribeTopic(final Context context, final String num) {
        YunBaManager.subscribe(context, num, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                //订阅成功
                MyUtils.showToast(context, "订阅成功");
                addTopic2ArrayLiat(num);
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                //订阅失败
                MyUtils.showToast(context, "订阅失败");

            }
        });
    }

    /**
     * 获取别名
     *
     * @param context
     * @return
     */
    public String getALias(Context context) {
        YunBaManager.getAlias(context, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                String alias=iMqttToken.getAlias();
                Log.i(Constant.TAG, "别名是：" + alias);
            }
            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
            }
        });
        return "";
    }

    /**
     * 取消订阅
     *
     * @param num
     */
    private void unSubsribe(final Context context, String num) {
        YunBaManager.unsubscribe(context, num, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                MyUtils.showToast(context, "取消订阅成功");
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                MyUtils.showToast(context, "取消订阅失败");
            }
        });
    }
    /**
     * 动态注册一个广播接收器
     */
    private void registerReciver() {
        mMessageReceiver=new MessageReceiver();
        IntentFilter filterReceiveMsg=new IntentFilter();
        filterReceiveMsg.addAction(YunBaManager.MESSAGE_RECEIVED_ACTION); // 获取消息的action
        filterReceiveMsg.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterReceiveMsg); // 注册一个收到消息的广播接收器

        IntentFilter filterConnect=new IntentFilter();
        filterConnect.addAction(YunBaManager.MESSAGE_CONNECTED_ACTION);
        filterConnect.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterConnect); // 注册一个消息连接的广播接收器

        IntentFilter filterDisconnect=new IntentFilter();
        filterDisconnect.addAction(YunBaManager.MESSAGE_DISCONNECTED_ACTION);
        filterDisconnect.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterDisconnect);

        IntentFilter filterPublishReceive=new IntentFilter();
        filterPublishReceive.addAction(YunBaManager.PUBLISH_RECEIVED_ACTION);
        filterPublishReceive.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterPublishReceive);

        IntentFilter filterPreSend=new IntentFilter(YunBaManager.PRESENCE_RECEIVED_ACTION);
        filterPreSend.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterPreSend);
    }

    /**
     * 内部类：MessageReceiver
     * 收到消息时的广播接收器类
     */
    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (YunBaManager.MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                //收到消息准备显示
                String topic=intent.getStringExtra(YunBaManager.MQTT_TOPIC);
                String msg=intent.getStringExtra(YunBaManager.MQTT_MSG);
                StringBuilder sb=new StringBuilder();
                String currentContent=tvShowMsg.getText().toString().trim();
                sb.append(currentContent);
                sb.append("[Receive Message from ").append(topic).append("] is ").append(msg).append("\n\n");
                showMessage(sb.toString());
            } else if (YunBaManager.MESSAGE_CONNECTED_ACTION.equals(intent.getAction())) {
                StringBuilder sb=new StringBuilder();
                sb.append("YUNBA-CONNECTED");
                setAppTitle(sb.toString());
            } else if (YunBaManager.MESSAGE_DISCONNECTED_ACTION.equals(intent.getAction())) {
                MyUtils.showToast(context, "断开链接了");
            } else if (YunBaManager.PUBLISH_RECEIVED_ACTION.equals(intent.getAction())) {
                String topic=intent.getStringExtra(YunBaManager.MQTT_TOPIC);
                String msg=intent.getStringExtra(YunBaManager.MQTT_MSG);
                StringBuilder sb=new StringBuilder();
                sb.append("[Message from").append(topic).append("]").append("is").append(msg);
                showMessage(sb.toString());

            } else if (YunBaManager.PRESENCE_RECEIVED_ACTION.equals(intent.getAction())) {
                String topic=intent.getStringExtra(YunBaManager.MQTT_TOPIC);
                String msg=intent.getStringExtra(YunBaManager.MQTT_MSG);
                StringBuilder sb=new StringBuilder();
                sb.append("[Message from presence").append(" topic is ]").append(topic).append("message is ").append(msg);
                showMessage(sb.toString());
            }
        }
    }

    /**
     * 设置消息显示
     *
     * @param msg
     */
    private void showMessage(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvShowMsg.setText(msg);
            }
        });
    }

    /**
     * 设置窗体标题
     *
     * @param title
     */
    public void setAppTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void addTopic2ArrayLiat(String topic) {
        for (String t : topicList) {
            if (topic.equals(t)) {
                return;
            }
            topicList.add(topic);
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
    }

    /**
     * 更新消息显示框
     * 更新所有缓存的消息
     */
    public void refreshLogView() {
        StringBuilder sb = new StringBuilder();
        if (Constant.msgList != null && Constant.msgList.size() > 0) {
            for (MyMessage msg : Constant.msgList) {
                sb.append("[Receive Message From ").append(msg.getTopic()).append("]").append(msg.getContent()).append("\n\n");
            }
            tvShowMsg.setText(sb.toString());
        }
    }
}


