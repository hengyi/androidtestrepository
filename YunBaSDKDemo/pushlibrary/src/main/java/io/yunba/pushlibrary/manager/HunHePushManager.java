package io.yunba.pushlibrary.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import com.huawei.android.pushagent.PushManager;
import com.xiaomi.mipush.sdk.MiPushClient;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import io.yunba.android.manager.YunBaManager;
import io.yunba.pushlibrary.constant.Constant;

/**
 * Author：林恒宜 on 16-8-22 09:34
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description : 混合推送的管理类
 */
public class HunHePushManager {
    /**
     * 注册小米和云吧的混合推送服务
     *
     * @param context  上下文
     * @param miAPPId  小米APP ID
     * @param miAPPKey 小米APP KEY
     */
    public static void registerYunBaAndXiaoMi(Context context, String miAPPId, String miAPPKey) {
        YunBaManager.start(context);
        MiPushClient.registerPush(context, miAPPId, miAPPKey);
    }

    /**
     * 注册小米和云吧的混合推送服务
     *
     * @param context     上下文
     * @param yunbaAppKey 云吧appkey
     * @param miAPPId     小米app id
     * @param miAPPKey    小米app key
     */
    public static void registerYunBaAndXiaoMi(Context context, String yunbaAppKey, String miAPPId, String miAPPKey) {
        YunBaManager.start(context, yunbaAppKey);
        MiPushClient.registerPush(context, miAPPId, miAPPKey);
    }

    /**
     * 注册云吧和华为的服务
     *
     * @param context 上下文
     */
    public static void registerYunBaAndHuaWei(Context context) {
        YunBaManager.start(context);
        PushManager.requestToken(context);
    }

    /**
     * 注册云吧和华为的服务
     *
     * @param context     上下文
     * @param yunbaAppKey 云吧的app key
     */
    public static void registerYunBaAndHuaWei(Context context, String yunbaAppKey) {
        YunBaManager.start(context, yunbaAppKey);
        PushManager.requestToken(context);
    }

    /**
     * 上报设备信息
     *
     * @param context   上下文
     * @param plantFrom 平台
     */
    public static void pushDeviceInfo(final Context context, final int plantFrom) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection conn;
                SharedPreferences config = context.getSharedPreferences("config", Context.MODE_PRIVATE);
                String yunbaUid = config.getString("yunbaUid", "");
                String xiaomiUid = config.getString("MI_REGID", "");
                String huaweiUid = config.getString("huawei_token", "");
//182.92.106.13
                try {
                    URL url = new URL("http://182.92.106.13:8080/upload");
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setConnectTimeout(5000);
                    conn.setReadTimeout(5000);

                    conn.setDoOutput(true);
                    conn.connect();

                    OutputStream outputStream = conn.getOutputStream();
                    Map<String, Object> infoMap = new HashMap<>();

                    switch (plantFrom) {
                        case 1:
                            infoMap.put("yunba_uid", yunbaUid);
                            infoMap.put("regid", xiaomiUid);
                            infoMap.put("platform", 1);
                            break;
                        case 2:
                            infoMap.put("yunba_uid", yunbaUid);
                            infoMap.put("tmid", huaweiUid);
                            infoMap.put("platform", 2);
                            break;
                    }
                    JSONObject jsonObject = new JSONObject(infoMap);
                    String jsonString = jsonObject.toString();
                    Log.i("输出", jsonString);
                    outputStream.write(jsonString.getBytes());
                    outputStream.flush();

                    if (conn.getResponseCode() == 200) {
                        Log.i(Constant.TAG, "push device information success");
                        InputStream is = conn.getInputStream();
                        OutputStream os = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024];
                        int length = -1;
                        while ((length = is.read(buffer)) != -1) {
                            os.write(buffer, 0, length);
                        }
                        os.flush();
                        os.close();
                        is.close();
                        Log.i(Constant.TAG, "返回的结果是：" + os.toString());
                    } else {
                        Log.i(Constant.TAG, "push device information failure");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("-----------------------------");
                    Map<String, Object> infoMap = new HashMap<>();

                    switch (plantFrom) {
                        case 1:
                            infoMap.put("yunba_uid", yunbaUid);
                            infoMap.put("regid", xiaomiUid);
                            infoMap.put("platform", 1);
                            break;
                        case 2:
                            infoMap.put("yunba_uid", yunbaUid);
                            infoMap.put("tmid", huaweiUid);
                            infoMap.put("platform", 2);
                            break;
                    }
                    JSONObject jsonObject = new JSONObject(infoMap);
                    String jsonString = jsonObject.toString();
                    Log.i("输出", jsonString);
                }
            }
        }).start();
    }

    /**
     * 获取并存储注册云吧服务的设备id
     *
     * @param context 上下文
     * @return 是否成功存储
     */
    public static boolean storeYunBaDeviceId(Context context) {
        try {
            String deviceId = Settings.System.getString(context.getContentResolver(), Constant.DEVICE_ID_KEY);//暂不用
            Log.i(Constant.TAG, "云吧的设备号是：" + deviceId);
            SharedPreferences config = context.getSharedPreferences("config", Context.MODE_PRIVATE);
            String userInfo = getString(context, Constant.USER_INFO_KEY, "");
            int index = userInfo.indexOf(Constant.USER_NAME_KEY);
            String userName = userInfo.substring(0, index);
            Log.i(Constant.TAG, "获取到的用户信息是：" + userName);
            config.edit().putString("yunbaUid", userName).commit(); // 把获取到的uid写入配置文件
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static SharedPreferences _pushPref = null;

    public static String getString(Context context, String key, String defValue) {
        getDefaultSharedPreferences(context);
        String retStr = _pushPref.getString(key, defValue);
        return retStr;
    }

    private static SharedPreferences getDefaultSharedPreferences(Context context) {
        if (null == _pushPref)
            _pushPref = context.getSharedPreferences(Constant.SERVER_CONFIG,
                    Context.MODE_PRIVATE);
        return _pushPref;
    }
}
