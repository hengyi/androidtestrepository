package io.yunba.pushlibrary.receiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

import io.yunba.pushlibrary.constant.Constant;
import io.yunba.pushlibrary.manager.HunHePushManager;

/**
 * Author：林恒宜 on 16-8-19 17:37
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description : 使用混合推送时自定义的广播接收器必须继承自该类，并且在重写onReceiveRegisterResult（）
 *              方法时一定要添加super.onReceiveRegisterResult(context, miPushCommandMessage);
 */
public class XiaoMiReceiver extends PushMessageReceiver {
    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onReceiveRegisterResult(context, miPushCommandMessage);
        // 获取小米的uid
        String regId = MiPushClient.getRegId(context);
        Log.i(Constant.TAG,"小米服务注册成功，注册得到的结果如下：" + regId);
        SharedPreferences config = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        config.edit().putString("MI_REGID",regId).commit();
        HunHePushManager.storeYunBaDeviceId(context);
        HunHePushManager.pushDeviceInfo(context, 1);
    }
}
