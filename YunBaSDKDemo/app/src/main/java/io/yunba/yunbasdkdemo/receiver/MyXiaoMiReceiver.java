package io.yunba.yunbasdkdemo.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;

import java.util.Random;

import io.yunba.pushlibrary.receiver.XiaoMiReceiver;
import io.yunba.yunbasdkdemo.MyApplication;
import io.yunba.yunbasdkdemo.R;
import io.yunba.yunbasdkdemo.activity.MainActivity;
import io.yunba.yunbasdkdemo.util.MyUtils;

/**
 * Author：林恒宜 on 16-8-24 10:49
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyXiaoMiReceiver extends XiaoMiReceiver {
    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onReceiveRegisterResult(context, miPushCommandMessage);

    }

    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceivePassThroughMessage(context, miPushMessage);
        String content = miPushMessage.getContent();
        if (MyUtils.isRunningForground(context)) {
            Handler handler = MyApplication.getMainActivity().getmHanlder();
            Message msg = handler.obtainMessage();
            msg.what = 3;
            String result = "小米:" + content;
            msg.obj = result;
            handler.sendMessage(msg);
        } else {

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("通知")
                    .setContentText(content)
                    .setAutoCancel(true);
            Intent intent = new Intent(context,MainActivity.class);
            intent.putExtra("msgContent",content);
            intent.putExtra("platform", 1);
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
            taskStackBuilder.addParentStack(MainActivity.class);
            taskStackBuilder.addNextIntent(intent);
            PendingIntent pi = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pi);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(new Random().nextInt(),mBuilder.build());
        }
    }

    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageClicked(context, miPushMessage);
        String messageContent = miPushMessage.getContent(); // 消息内容

        if (MyUtils.isRunningForground(context)) {
            Handler handler = MyApplication.getMainActivity().getmHanlder();
            Message msg = handler.obtainMessage();
            msg.what = 3;
            String result = "小米:" + messageContent;
            msg.obj = result;
            handler.sendMessage(msg);
        } else {
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("msgContent",messageContent);
            intent.putExtra("platform", 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }



}
