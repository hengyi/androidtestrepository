package io.yunba.yunbasdkdemo.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.yunba.android.manager.YunBaManager;
import io.yunba.pushlibrary.constant.Constant;
import io.yunba.yunbasdkdemo.MyApplication;
import io.yunba.yunbasdkdemo.R;
import io.yunba.yunbasdkdemo.util.MyUtils;

public class MainActivity extends AppCompatActivity {

    private MessageReceiver mMessageReceiver;
    private EditText etAlias;
    private EditText etMsg;
    private EditText etSubscribeNum;
    private EditText etTopic;
    private Button btnSelectTopic;
    private Button btnSendMessage;
    private Button btnSetAlias;
    private Button btnSubscribe;
    private Button btnGetAlias;
    private Button btnUnSubscribe;
    private LinearLayout llShowMsg;

    private String mTopic;
    private String mMessage;
    private String mAlias;
    private String mSubTopic;

    private static final int SEND_MESSAGE_SUCCESS = 0;
    private static final int SET_ALIAS_SUCCESS = 1;
    private static final int GET_ALIAS_SUCCESS = 2;
    private static final int SHOW_MESSAGE = 3;


    private Handler mHanlder = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SEND_MESSAGE_SUCCESS:
                    String sendMessage = (String) msg.obj;
                    TextView tvUserMsg = new TextView(getApplicationContext());
                    tvUserMsg.setTextSize(16);
                    tvUserMsg.setTextColor(Color.BLUE);
                    tvUserMsg.setText("【Success To Send Message:】" + sendMessage);

                    TextView tvTime = new TextView(getApplicationContext());
                    tvTime.setTextSize(12);
                    tvTime.setTextColor(Color.BLUE);
                    String pattern = "yyyy-MM-dd HH:mm:ss";
                    DateFormat format = new SimpleDateFormat(pattern);
                    String time = format.format(new Date(System.currentTimeMillis()));
                    tvTime.setText(time);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 25, 0); // 参数分别为左上右下
                    params.gravity = Gravity.RIGHT;
                    tvUserMsg.setLayoutParams(params);
                    tvTime.setLayoutParams(params);
                    llShowMsg.addView(tvUserMsg);
                    llShowMsg.addView(tvTime);
                    etMsg.setText("");
                    break;
                case SET_ALIAS_SUCCESS:
                    String alias = (String) msg.obj;
                    TextView tvAlias = new TextView(getApplicationContext());
                    tvAlias.setTextSize(16);
                    tvAlias.setTextColor(Color.BLUE);
                    tvAlias.setText("【设置别名成功】" + alias);
                    tvAlias.setGravity(Gravity.CENTER_HORIZONTAL);
                    llShowMsg.addView(tvAlias);
                    etAlias.setText("");
                    break;
                case GET_ALIAS_SUCCESS:
                    String getAlias = (String) msg.obj;
                    TextView tvShowAlias = new TextView(getApplicationContext());
                    tvShowAlias.setTextSize(16);
                    tvShowAlias.setTextColor(Color.BLUE);
                    tvShowAlias.setText("【获取别名成功】" + getAlias);
                    tvShowAlias.setGravity(Gravity.CENTER_HORIZONTAL);
                    llShowMsg.addView(tvShowAlias);
                    break;
                case SHOW_MESSAGE:
                    String result = (String) msg.obj;
                    int index = result.indexOf(":");
                    String topic = result.substring(0, index);
                    String message = result.substring(index + 1);

                    TextView tvShowMsgUserMsg = new TextView(getApplicationContext());
                    tvShowMsgUserMsg.setTextSize(16);
                    tvShowMsgUserMsg.setTextColor(Color.BLUE);
                    tvShowMsgUserMsg.setText("【Receive Message From Topic:" + topic + "is :】" + message);

                    TextView tvShowTime = new TextView(getApplicationContext());
                    tvShowTime.setTextSize(12);
                    tvShowTime.setTextColor(Color.BLUE);
                    String showMsgPattern = "yyyy-MM-dd HH:mm:ss";
                    DateFormat showFormat = new SimpleDateFormat(showMsgPattern);
                    String time1 = showFormat.format(new Date(System.currentTimeMillis()));
                    tvShowTime.setText(time1);

                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params1.setMargins(0, 0, 25, 0); // 参数分别为左上右下
                    params1.gravity = Gravity.LEFT;
                    tvShowMsgUserMsg.setLayoutParams(params1);
                    tvShowTime.setLayoutParams(params1);
                    llShowMsg.addView(tvShowMsgUserMsg);
                    llShowMsg.addView(tvShowTime);
                    etMsg.setText("");
                    break;
            }
        }
    };

    public Handler getmHanlder() {
        return this.mHanlder;
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnSelectTopic:
                    selectTopic(getApplicationContext());
                    break;
                case R.id.btnSendMsg:
                    mMessage = etMsg.getText().toString().trim();
                    sendMsg(getApplicationContext(), mTopic, mMessage);
                    break;
                case R.id.btnSetAlias:
                    mAlias = etAlias.getText().toString().trim();
                    setAlias(getApplicationContext(), mAlias);
                    break;
                case R.id.btnSubscribe:
                    mSubTopic = etSubscribeNum.getText().toString().trim();
                    subscribeTopic(getApplicationContext(), mSubTopic);
                    break;
                case R.id.btnGetAlias:
                    getALias(getApplicationContext());
                    break;
                case R.id.btnUnSubscribe:
                    mSubTopic = etSubscribeNum.getText().toString().trim();
                    unSubsribe(getApplicationContext(), mSubTopic);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.setMainActivity(this);
        Intent intent = getIntent();
        String messageContent = intent.getStringExtra("msgContent");
        int platform = intent.getIntExtra("platform",0);
        if (platform != 0) {
            if (platform == 1) {
                if (intent != null && !TextUtils.isEmpty(messageContent)) {
                    showMessage(getApplicationContext(), "小米", messageContent);
                }
            }
            if (platform == 2) {
                if (intent != null && !TextUtils.isEmpty(messageContent)) {
                    showMessage(getApplicationContext(), "华为", messageContent);
                }
            }

        }
        initUI();
        initLinstener();
        registerReciver();
        showInfo();
    }

    private void showInfo() {
        SharedPreferences config = getSharedPreferences("config", MODE_PRIVATE);
        String manufacture = config.getString("manufacture", "");
        if (manufacture.equalsIgnoreCase("xiaomi")) {
            String miRegid = config.getString("MI_REGID", "");
            String yunbaUid = config.getString("yunbaUid", "");

            TextView tvUserMsg = new TextView(getApplicationContext());
            tvUserMsg.setTextSize(16);
            tvUserMsg.setTextColor(Color.BLUE);
            tvUserMsg.setText("【注册小米成功】" + miRegid + "\n\n" + "【注册云吧成功】" + yunbaUid);

            llShowMsg.addView(tvUserMsg);

        } else if (manufacture.equalsIgnoreCase("huawei")) {
            String yunbaUid = config.getString("yunbaUid", "");
            String tmId = config.getString("huawei_token", "");

            TextView tvUserMsg = new TextView(getApplicationContext());
            tvUserMsg.setTextSize(16);
            tvUserMsg.setTextColor(Color.BLUE);
            tvUserMsg.setText("【注册华为成功】" + tmId + "\n\n" + "【注册云吧成功】" + yunbaUid);

            llShowMsg.addView(tvUserMsg);
        } else {
            String yunbaUid = config.getString("yunbaUid", "");
            TextView tvUserMsg = new TextView(getApplicationContext());
            tvUserMsg.setTextSize(16);
            tvUserMsg.setTextColor(Color.BLUE);
            tvUserMsg.setText("【注册云吧成功】" + yunbaUid);
            llShowMsg.addView(tvUserMsg);
        }
    }

    private void initLinstener() {
        btnGetAlias.setOnClickListener(listener);
        btnSelectTopic.setOnClickListener(listener);
        btnSendMessage.setOnClickListener(listener);
        btnSetAlias.setOnClickListener(listener);
        btnSubscribe.setOnClickListener(listener);
        btnUnSubscribe.setOnClickListener(listener);
    }

    private void initUI() {
        etAlias = (EditText) findViewById(R.id.etAlias);
        etMsg = (EditText) findViewById(R.id.etMsg);
        etSubscribeNum = (EditText) findViewById(R.id.etSubscribeNum);
        etTopic = (EditText) findViewById(R.id.etTopic);

        btnSelectTopic = (Button) findViewById(R.id.btnSelectTopic);
        btnSendMessage = (Button) findViewById(R.id.btnSendMsg);
        btnSetAlias = (Button) findViewById(R.id.btnSetAlias);
        btnSubscribe = (Button) findViewById(R.id.btnSubscribe);
        btnGetAlias = (Button) findViewById(R.id.btnGetAlias);
        btnUnSubscribe = (Button) findViewById(R.id.btnUnSubscribe);

        llShowMsg = (LinearLayout) findViewById(R.id.llShowMsg);
    }

    /**
     * 弹出选择发送频道号的对话框
     */
    private void selectTopic(Context context) {
        mTopic = etTopic.getText().toString().trim();
        if (TextUtils.isEmpty(mTopic)) {
            Toast.makeText(context, "频道不能为空", Toast.LENGTH_SHORT).show();
        } else {
            TextView tvTopic = new TextView(context);
            tvTopic.setTextSize(20);
            tvTopic.setTextColor(Color.BLUE);
            tvTopic.setText("Topic:" + mTopic);
            tvTopic.setGravity(Gravity.CENTER_HORIZONTAL);
            llShowMsg.addView(tvTopic);
            etTopic.setText("");
        }

    }

    /**
     * 发送消息
     *
     * @param context 运行上下文
     * @param topic   要发送到的频道号
     * @param message     待发送的消息
     */
    private void sendMsg(final Context context, final String topic, final String message) {
        if (TextUtils.isEmpty(topic)) {
            Toast.makeText(context, "频道还没有设置，请先设置再发消息", Toast.LENGTH_SHORT).show();
        } else {
            if (TextUtils.isEmpty(message)) {
                Toast.makeText(context, "消息体不能为空", Toast.LENGTH_SHORT).show();
            } else {
                YunBaManager.publish(context, topic, message, new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken iMqttToken) {
                        Message msg = mHanlder.obtainMessage();
                        msg.what = SEND_MESSAGE_SUCCESS;
                        msg.obj = message;
                        mHanlder.sendMessage(msg);
                    }

                    @Override
                    public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                        MyUtils.showToast(context, "消息发送失败");
                    }
                });
            }
        }

    }

    /**
     * 设置别名
     *
     * @param alias 别名
     */
    private void setAlias(final Context context, final String alias) {
        YunBaManager.setAlias(context, alias, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                Message message = mHanlder.obtainMessage();
                message.what = SET_ALIAS_SUCCESS;
                message.obj = alias;
                mHanlder.sendMessage(message);
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                MyUtils.showToast(context, "设置别名失败");
            }
        });
    }

    /**
     * 订阅频道号
     *
     * @param context 上下文环境
     * @param num     待订阅的频道号
     */
    private void subscribeTopic(final Context context, final String num) {
        YunBaManager.subscribe(context, num, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                //订阅成功
                MyUtils.showToast(context, "订阅成功");

            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                //订阅失败
                MyUtils.showToast(context, "订阅失败");

            }
        });
    }

    /**
     * 获取别名
     *
     * @param context
     * @return
     */
    public String getALias(final Context context) {
        YunBaManager.getAlias(context, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                String alias = iMqttToken.getAlias();
                Log.i(Constant.TAG, "别名是：" + alias);
                Message msg = mHanlder.obtainMessage();
                msg.what = GET_ALIAS_SUCCESS;
                msg.obj = alias;
                mHanlder.sendMessage(msg);

            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
            }
        });
        return "";
    }

    /**
     * 取消订阅
     *
     * @param num
     */
    private void unSubsribe(final Context context, String num) {
        YunBaManager.unsubscribe(context, num, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                MyUtils.showToast(context, "取消订阅成功");
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                MyUtils.showToast(context, "取消订阅失败");
            }
        });
    }

    /**
     * 动态注册一个广播接收器
     */
    private void registerReciver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filterReceiveMsg = new IntentFilter();
        filterReceiveMsg.addAction(YunBaManager.MESSAGE_RECEIVED_ACTION); // 获取消息的action
        filterReceiveMsg.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterReceiveMsg); // 注册一个收到消息的广播接收器

        IntentFilter filterConnect = new IntentFilter();
        filterConnect.addAction(YunBaManager.MESSAGE_CONNECTED_ACTION);
        filterConnect.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterConnect); // 注册一个消息连接的广播接收器

        IntentFilter filterDisconnect = new IntentFilter();
        filterDisconnect.addAction(YunBaManager.MESSAGE_DISCONNECTED_ACTION);
        filterDisconnect.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterDisconnect);

        IntentFilter filterPublishReceive = new IntentFilter();
        filterPublishReceive.addAction(YunBaManager.PUBLISH_RECEIVED_ACTION);
        filterPublishReceive.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterPublishReceive);

        IntentFilter filterPreSend = new IntentFilter(YunBaManager.PRESENCE_RECEIVED_ACTION);
        filterPreSend.addCategory(getPackageName());
        registerReceiver(mMessageReceiver, filterPreSend);
    }

    /**
     * 内部类：MessageReceiver
     * 收到消息时的广播接收器类
     */
    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (YunBaManager.MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                //收到消息准备显示
                String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
                String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
                showMessage(context, topic, msg);
            } else if (YunBaManager.MESSAGE_CONNECTED_ACTION.equals(intent.getAction())) {
                StringBuilder sb = new StringBuilder();
                sb.append("YUNBA-CONNECTED");
                setAppTitle(sb.toString());
            } else if (YunBaManager.MESSAGE_DISCONNECTED_ACTION.equals(intent.getAction())) {
                MyUtils.showToast(context, "断开链接了");
            } else if (YunBaManager.PUBLISH_RECEIVED_ACTION.equals(intent.getAction())) {
                String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
                String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
                showMessage(context, topic, msg);
            } else if (YunBaManager.PRESENCE_RECEIVED_ACTION.equals(intent.getAction())) {
                String topic = intent.getStringExtra(YunBaManager.MQTT_TOPIC);
                String msg = intent.getStringExtra(YunBaManager.MQTT_MSG);
                showMessage(context, topic, msg);
            }
        }
    }

    /**
     * 设置消息显示
     *
     * @param context 上下文
     * @param topic   频道
     * @param message 消息体
     */
    private void showMessage(final Context context, final String topic, final String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = mHanlder.obtainMessage();
                msg.what = SHOW_MESSAGE;
                msg.obj = topic + ":" + message;
                mHanlder.sendMessage(msg);
            }
        }).start();
    }

    /**
     * 设置窗体标题
     *
     * @param title
     */
    public void setAppTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
    }
}
