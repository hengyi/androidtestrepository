package io.yunba.yunbasdkdemo.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import io.yunba.pushlibrary.constant.Constant;
import io.yunba.pushlibrary.receiver.HuaWeiReceiver;
import io.yunba.yunbasdkdemo.MyApplication;
import io.yunba.yunbasdkdemo.R;
import io.yunba.yunbasdkdemo.activity.MainActivity;
import io.yunba.yunbasdkdemo.constant.Constants;
import io.yunba.yunbasdkdemo.util.MyUtils;

/**
 * Author：林恒宜 on 16-8-24 10:50
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyHuaWeiReceiver extends HuaWeiReceiver {
    @Override
    public void onToken(Context context, String token, Bundle bundle) {
        super.onToken(context, token, bundle);
    }
    /**
     * 描述：推送消息下来时会自动回调onPushMsg方法实现应用透传消息处理。本接口必须被实现
     *
     * @param context  程序运行的上下文
     * @param msgBytes 透传消息字节数组
     * @param extras   扩展信息暂时不启用
     * @return
     */
    @Override
    public boolean onPushMsg(Context context, byte[] msgBytes, Bundle extras) {
        String messageContent = null;
        try {
            messageContent = new String(msgBytes,"UTF-8");
            Log.i(Constant.TAG, "收到一条消息：" + messageContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (MyUtils.isRunningForground(context)) {
            Handler handler = MyApplication.getMainActivity().getmHanlder();
            Message msg = handler.obtainMessage();
            msg.what = 3;
            String result = "华为:" + messageContent;
            msg.obj = result;
            handler.sendMessage(msg);
        } else {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("通知")
                    .setContentText(messageContent)
                    .setAutoCancel(true);

            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("msgContent",messageContent);
            intent.putExtra("platform", 2);
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
            taskStackBuilder.addParentStack(MainActivity.class);
            taskStackBuilder.addNextIntent(intent);
            PendingIntent pi = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pi);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(new Random().nextInt(), mBuilder.build());
        }
        return true;
    }

    /**
     * 实现业务事件。该方法会在设置标签、LBS信息之后、点击打开通知栏消息、点击通知栏上的按钮之后被调用。
     * 由业务决定是否调用该函数。
     *
     * @param context 程序运行的上下文
     * @param event   事件类型 event为枚举类型,事件定义如下
     *                Event定义事件如下:
     *                public static enum Event
     *                {
     *                NOTIFICATION_OPENED，      //通知栏中的通知被点击打开
     *                NOTIFICATION_CLICK_BTN,   //通知栏中通知上的按钮被点击
     *                PLUGINRSP,                //标签上报回应
     *                }
     * @param extras  扩展信息extras为携带的参数,应用可根据不同事件获取不同参数作处理。可参看SDK文档来处理
     */
    @Override
    public void onEvent(Context context, Event event, Bundle extras) {

        //标签上报回应
        if (Event.PLUGINRSP.equals(event)) {
            /*
             * 该类型下extras 参数
             * （1）BOUND_KEY.PLUGINREPORTTYPE 上报类型,1为LBS(当前暂不支持),2为标签。
             * （2）BOUND_KEY.PLUGINREPORTRESULT 上报结果,成功则为true,默认为false。
             */
            final int TYPE_LBS = 1; // 当前暂不支持
            final int TYPE_TAG = 2; // 标签
            int type = extras.getInt(BOUND_KEY.PLUGINREPORTTYPE, -1);
            boolean isSuccess = extras.getBoolean(BOUND_KEY.PLUGINREPORTRESULT, false);
            StringBuilder message = new StringBuilder();
            if (type == TYPE_LBS) {
                message.append("LBS Result is :");
            } else if (type == TYPE_TAG) {
                message.append("LAG Result is :");
            }
            showPushMessage(Constants.SET_TOPIC_SUCCESS_CODE,"[" + getCurrentTime() +"]" + message.toString() + isSuccess + "\n\n");

        }
        //通知栏中通知上的按钮被点击
        else if (Event.NOTIFICATION_CLICK_BTN.equals(event) || Event.NOTIFICATION_OPENED.equals(event)) {
            Log.i(Constant.TAG, Constants.RECEIVE_PUSH_MESSAGE_SUCCESS + "notification_click_btn");
            /*
             * 该类型下extras 参数
             * （1）BOUND_KEY.pushMsgKey   匹配该字符串以决定点击按钮后处理何种事件
             * （2）BOUND_KEY.pushNotifyId 通知栏id,点击通知栏上的按钮后,业务可根据需要清除掉该通知栏或保留;默认为保留。
             */
            int notifyId = extras.getInt(BOUND_KEY.pushNotifyId, -1);
            if (notifyId != -1) {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(notifyId);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("收到的消息是：").append(extras.getString(BOUND_KEY.pushMsgKey, ""));
            //showPushMessage(Constant.RECEIVE_PUSH_MESSAGE_SUCCESS_CODE,getCurrentTime() + sb.toString() +"\n\n");
        }
        //通知栏中的通知被点击打开
//        else if (Event.NOTIFICATION_OPENED.equals(event)) {
//            Log.i(Constant.TAG,Constant.RECEIVE_PUSH_MESSAGE_SUCCESS + "notification_opened"); // 不一定会回调该方法
//            /*
//             * 该类型下extras 参数
//             * （1）BOUND_KEY.pushMsgKey 附加信息,由业务自行处理。 需要在华为开发者联盟上推送消息时添加自定义键值对才回调。
//             */
//        }
        super.onEvent(context, event, extras);
    }

    /**
     * 显示推送消息,在该方法里主要就是实现发送消息，让MainActivity去处理消息
     *
     * @param type    消息的类型
     * @param message 消息的内容
     */
    private void showPushMessage(int type, String message) {

    }

    /**
     * 获取当前时间并格式化
     * @return 格式化的时间
     */
    private String getCurrentTime() {
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        String currentTime = format.format(new Date());
        return currentTime;
    }
}
