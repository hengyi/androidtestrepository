package io.yunba.yunbasdkdemo;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.util.Log;

import java.util.List;

import io.yunba.android.manager.YunBaManager;
import io.yunba.pushlibrary.constant.Constant;
import io.yunba.pushlibrary.manager.HunHePushManager;
import io.yunba.yunbasdkdemo.activity.MainActivity;
import io.yunba.yunbasdkdemo.constant.Constants;

/**
 * Author：林恒宜 on 16-8-24 10:37
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyApplication extends Application {
    private static MainActivity mainActivity;

    public static void setMainActivity(MainActivity mainActivity) {
        MyApplication.mainActivity = mainActivity;
    }

    public static MainActivity getMainActivity() {
        return MyApplication.mainActivity;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        if (shouldInit()) {
            String manufacture = Build.MANUFACTURER;
            getSharedPreferences("config",MODE_PRIVATE).edit().putString("manufacture",manufacture).commit();
            if (manufacture.equals("Xiaomi")) {
                Log.i(Constant.TAG, "确认是小米手机...开始连接云吧和小米服务");
                HunHePushManager.registerYunBaAndXiaoMi(this, Constants.MI_APP_ID,Constants.MI_APP_KEY);
            } else if (manufacture.equalsIgnoreCase("huawei")){
                Log.i(Constant.TAG, "确认是华为手机...开始连接云吧和华为服务....");
                HunHePushManager.registerYunBaAndHuaWei(this);
            } else {
                Log.i(Constants.DEBUG_TAG,"其他手机连接云吧的服务");
                YunBaManager.start(this);
            }
        }
    }

    /**
     * 由于有多线程所以在处理时需要判断是不是多需要初始化
     * @return true：需要初始化，false：不能初始化
     */
    private boolean shouldInit() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }
}
