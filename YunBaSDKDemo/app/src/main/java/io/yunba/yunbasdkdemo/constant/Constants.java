package io.yunba.yunbasdkdemo.constant;

/**
 * Author：林恒宜 on 16-8-24 10:38
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class Constants {
    public static final String DEBUG_TAG = "debug_tag";

    public static final String MI_APP_KEY = "5851750457115";
    public static final String MI_APP_ID = "2882303761517504115";

    public static final String HUAWEI_APP_ID = "10630523";
    public static final String HUAWEI_APP_SECRET = "441cdfb31e3fcb3f60d4173b5abfc48f";

    public static final String REQUEST_TOKEN_SUCCESS = "request_token_success";
    public static final String SET_TOPIC_SUCCESS = "set_topic_success";
    public static final String RECEIVE_PUSH_MESSAGE_SUCCESS = "receive_push_message_success";
    public static final int REQUEST_TOKEN_SUCCESS_CODE = 0; // 注册成功
    public static final int SET_TOPIC_SUCCESS_CODE     = 1; // 设置频道成功
    public static final int RECEIVE_PUSH_MESSAGE_SUCCESS_CODE = 2;// 收到推送消息成功
}
