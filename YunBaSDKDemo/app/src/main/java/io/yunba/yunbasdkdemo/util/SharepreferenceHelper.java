package io.yunba.yunbasdkdemo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import io.yunba.pushlibrary.constant.Constant;


/**
 * Author：林恒宜 on 16-8-2 14:53
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class SharepreferenceHelper {
    private static SharedPreferences preferences;
    private static String config = "config";

    public static String getString(Context context, String key, String defaultValue) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        String result = preferences.getString(key, defaultValue);
        return result;
    }

    public static int getInteger(Context context, String key, int defaultValue) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        int result = preferences.getInt(key, defaultValue);
        return result;
    }

    public static float getFloat(Context context, String key, float defaultValue) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        float result = preferences.getFloat(key, defaultValue);
        return result;
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        boolean result = preferences.getBoolean(key, defaultValue);
        return result;
    }

    public static void setString(Context context, String key, String value) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(key, value);
        edit.commit();
        Log.i(Constant.TAG, "SharedPreferences -> setString -> success");
    }

    public static void setInteger(Context context, String key, int value) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public static void setFloat(Context context, String key, float value) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putFloat(key, value);
        edit.commit();
    }

    public static void setBoolean(Context context, String key, boolean value) {
        preferences = context.getSharedPreferences(config, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }
}
