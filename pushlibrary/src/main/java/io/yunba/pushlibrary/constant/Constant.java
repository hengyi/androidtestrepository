package io.yunba.pushlibrary.constant;

/**
 * Author：林恒宜 on 16-8-19 17:09
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class Constant {
    public static final String TAG = "yunba_mix_push_logcat:";
    public static final String HUA_WEI_TOKEN = "huawei_token";
    public static final String DEVICE_ID_KEY = "MSBUS_DeviceId";
    public static final String USER_INFO_KEY = "u_p_c";
    public static final String USER_NAME_KEY = "$$";
    public static final String SERVER_CONFIG = "mqpush.serverconfig";
}
