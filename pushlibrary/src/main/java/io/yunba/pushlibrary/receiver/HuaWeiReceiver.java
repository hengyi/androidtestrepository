package io.yunba.pushlibrary.receiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.huawei.android.pushagent.api.PushEventReceiver;

import io.yunba.pushlibrary.constant.Constant;
import io.yunba.pushlibrary.manager.HunHePushManager;

/**
 * Author：林恒宜 on 16-8-19 17:05
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description : 使用混合推送时自定义的广播接收器必须继承自该类，并且在重写onToken（）
 *              方法时一定要添加super.onToken(context, token, bundle);
 */
public class HuaWeiReceiver extends PushEventReceiver {
    @Override
    public void onToken(Context context, String token, Bundle bundle) {
        Log.i(Constant.TAG, "注册华为Token成功！Token = " + token);
        SharedPreferences config = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        config.edit().putString(Constant.HUA_WEI_TOKEN,token).commit();
        HunHePushManager.storeYunBaDeviceId(context);
        HunHePushManager.pushDeviceInfo(context,2);
        super.onToken(context, token, bundle);
    }
}
