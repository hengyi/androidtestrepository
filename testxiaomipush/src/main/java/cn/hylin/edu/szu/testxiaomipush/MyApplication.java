package cn.hylin.edu.szu.testxiaomipush;

import android.app.ActivityManager;
import android.app.Application;
import android.os.Handler;
import android.os.Message;
import android.os.Process;

import com.xiaomi.mipush.sdk.MiPushClient;

import java.util.List;

import cn.hylin.edu.szu.testxiaomipush.activity.MainActivity;
import cn.hylin.edu.szu.testxiaomipush.bean.Constant;

/**
 * Author：林恒宜 on 16-8-4 11:06
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyApplication extends Application {

    private static MainActivity mActivity = null;
    private static MyHanlder mHanlder = null;

    public static void setmActivity(MainActivity mActivity) {
        MyApplication.mActivity = mActivity;
    }

    public static MyHanlder getMHanlder () {
        return mHanlder;
    }

    public static class MyHanlder extends Handler {
        public MyHanlder() {
        }
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                mActivity.refreshLogView(); // 更新界面
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (shouldInit()) {
            // 注册小米推送服务，注册成功会在MainReceiver的onReceiveRegisterResult中返回注册码
            MiPushClient.registerPush(this, Constant.APPID,Constant.APPKEY);
        }

        if (mHanlder == null) {
            mHanlder = new MyHanlder(); // 初始化一个消息处理器
        }
    }

    /**
     * 判断是否需要初始化
     * 因为有服务会在新进程中运行，所以会导致MyApplication被重复初始化，
     * 因此需要限制只允许让主进程初始化小米推送服务
     * @return 是否需要初始化 true表示需要初始化 false表示不需要初始化
     */
    private boolean shouldInit() {
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainThreadName = getPackageName();
        int myId = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myId && mainThreadName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }
}
