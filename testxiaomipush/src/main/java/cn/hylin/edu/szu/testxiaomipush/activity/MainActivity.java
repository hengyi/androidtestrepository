package cn.hylin.edu.szu.testxiaomipush.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xiaomi.mipush.sdk.MiPushClient;

import cn.hylin.edu.szu.testxiaomipush.MyApplication;
import cn.hylin.edu.szu.testxiaomipush.R;
import cn.hylin.edu.szu.testxiaomipush.bean.Constant;
import cn.hylin.edu.szu.testxiaomipush.bean.MyMessage;
import cn.hylin.edu.szu.testxiaomipush.view.TimeIntervalDialog;

public class MainActivity extends AppCompatActivity {

    private TextView tvShowMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.setmActivity(this);

        tvShowMsg = (TextView) findViewById(R.id.tvShowMsg);

        //设置别名
        findViewById(R.id.btnTestSetAlias).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请输入别名")
                        .setView(editText)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String alias = editText.getText().toString().trim();
                                if (TextUtils.isEmpty(alias)) {
                                    Toast.makeText(MainActivity.this, "没有输入别名，请重新输入", Toast.LENGTH_SHORT).show();
                                } else {
                                    MiPushClient.setAlias(getApplicationContext(), alias, null);
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });
        //设置账号
        findViewById(R.id.btnTestSetUserAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请输入账号")
                        .setView(editText)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String userAccount = editText.getText().toString().trim();
                                if (TextUtils.isEmpty(userAccount)) {
                                    Toast.makeText(MainActivity.this, "没有输入账号，请重新输入", Toast.LENGTH_SHORT).show();
                                } else {
                                    MiPushClient.setUserAccount(getApplicationContext(), userAccount, null);
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });

        //订阅主题
        findViewById(R.id.btnTestSubscribeTpoic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请输入需要订阅的主题")
                        .setView(editText)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String topic = editText.getText().toString().trim();
                                if (TextUtils.isEmpty(topic)) {
                                    Toast.makeText(MainActivity.this, "没有输入主题，请重新输入", Toast.LENGTH_SHORT).show();
                                } else {
                                    MiPushClient.subscribe(getApplicationContext(), topic, null);
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });

        //取消别名
        findViewById(R.id.btnTestUnSetAlias).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请输入需要取消的别名")
                        .setView(editText)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String alias = editText.getText().toString().trim();
                                if (TextUtils.isEmpty(alias)) {
                                    Toast.makeText(MainActivity.this, "没有输入别名，请重新输入", Toast.LENGTH_SHORT).show();
                                } else {
                                    MiPushClient.unsetAlias(getApplicationContext(), alias, null);
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });

        //取消账号
        findViewById(R.id.btnTestSetUnUserAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请输入需要取消的账号")
                        .setView(editText)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String userAccount = editText.getText().toString().trim();
                                if (TextUtils.isEmpty(userAccount)) {
                                    Toast.makeText(MainActivity.this, "没有输入账号，请重新输入", Toast.LENGTH_SHORT).show();
                                } else {
                                    MiPushClient.unsetUserAccount(getApplicationContext(), userAccount, null);
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });
        //取消订阅
        findViewById(R.id.btnTestUnSubscribeTpoic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editText = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("请输入需要取消订阅的主题")
                        .setView(editText)
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String topic = editText.getText().toString().trim();
                                if (TextUtils.isEmpty(topic)) {
                                    Toast.makeText(MainActivity.this, "没有输入主题，请重新输入", Toast.LENGTH_SHORT).show();
                                } else {
                                    MiPushClient.unsubscribe(getApplicationContext(), topic, null);
                                }
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });
        //暂停接收推送
        findViewById(R.id.btnTestPaushPush).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MiPushClient.pausePush(getApplicationContext(), null);
            }
        });
        //恢复接收推送
        findViewById(R.id.btnTestResumePush).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MiPushClient.resumePush(getApplicationContext(), null);
            }
        });
        //设置时间
        findViewById(R.id.btnTestSetTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //弹出设置时间的组件
                new TimeIntervalDialog(MainActivity.this, new TimeIntervalDialog.TimeIntervalInterface() {

                    @Override
                    public void apply(int startHour, int startMin, int endHour,
                                      int endMin) {
                        MiPushClient.setAcceptTime(MainActivity.this, startHour, startMin, endHour, endMin, null);
                    }
                    @Override
                    public void cancel() {
                    }

                }).show();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    /**
     * 更新消息显示框
     * 更新所有缓存的消息
     */
    public void refreshLogView() {
        StringBuilder sb = new StringBuilder();
        if (Constant.msgList != null && Constant.msgList.size() > 0) {
            for (MyMessage msg : Constant.msgList) {
                sb.append("[Receive Message From ").append(msg.getTopic()).append("]").append(msg.getContent()).append("\n\n");
            }
            tvShowMsg.setText(sb.toString());
        }
    }

}
