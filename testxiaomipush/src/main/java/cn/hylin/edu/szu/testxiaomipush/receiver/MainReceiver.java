package cn.hylin.edu.szu.testxiaomipush.receiver;

import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

import java.util.List;

import cn.hylin.edu.szu.testxiaomipush.MyApplication;
import cn.hylin.edu.szu.testxiaomipush.bean.Constant;
import cn.hylin.edu.szu.testxiaomipush.bean.MyMessage;
import cn.hylin.edu.szu.testxiaomipush.utils.MyUtils;

public class MainReceiver extends PushMessageReceiver {
    public MainReceiver() {
    }
    /**
     * 处理透传信息
     * 把消息取出后封装到常量类的消息列表中去
     * 由MainActivity来处理刷新消息列表
     * @param context 程序运行的上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceivePassThroughMessage(context, miPushMessage);
        Log.i(Constant.TAG,Constant.RECEIVE_THROUGH_MESSAGE_FROM_SERVER);
        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic();
        MyMessage myMessage = new MyMessage(messageTopic,messageContent);
        Constant.msgList.add(0,myMessage);
        Message msg = Message.obtain();
        msg.what =1;
        MyApplication.getMHanlder().sendMessage(msg);
    }

    /**
     * 处理收到的通知栏消息点击事件
     * @param context 程序运行的上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageClicked(context, miPushMessage);
        Log.i(Constant.TAG,Constant.RECEIVE_NOTIFICATION_MESSAGE_SUCCESS);

        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic();

        MyMessage myMessage = new MyMessage(messageTopic,messageContent);
        Constant.msgList.add(0,myMessage);
        Message msg = Message.obtain();
        msg.obj = myMessage;
        MyApplication.getMHanlder().sendMessage(msg);
    }

    /**
     * 处理点击通知栏消息
     * @param context 程序运行的上下文
     * @param miPushMessage 返回的消息对象
     */
    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageArrived(context, miPushMessage);
        Log.i(Constant.TAG,Constant.RECEIVE_THROUGH_MESSAGE_FROM_SERVER);
        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic();
        MyMessage myMessage = new MyMessage(messageTopic,messageContent);
        Constant.msgList.add(0,myMessage);
        Message msg = Message.obtain();
        msg.obj = myMessage;
        MyApplication.getMHanlder().sendMessage(msg);
    }

    @Override
    public void onReceiveMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceiveMessage(context, miPushMessage);

        Log.i(Constant.TAG,Constant.RECEIVE_THROUGH_MESSAGE_FROM_SERVER);
        String messageContent = miPushMessage.getContent(); // 消息内容
        String messageTopic = miPushMessage.getTopic();
        MyMessage myMessage = new MyMessage(messageTopic,messageContent);
        Constant.msgList.add(0,myMessage);
        Message msg = Message.obtain();
        msg.obj = myMessage;
        MyApplication.getMHanlder().sendMessage(msg);
    }

    /**
     * 处理收到注册命令返回的消息
     * @param context 程序运行的上下文
     * @param miPushCommandMessage 返回的消息对象
     */
    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onReceiveRegisterResult(context, miPushCommandMessage);
        List<String> arguments = miPushCommandMessage.getCommandArguments();
        String cmdArg1 = (arguments != null && arguments.size() > 0) ? arguments.get(0) : null ;
        String cmdArg2 = (arguments != null && arguments.size() > 1) ? arguments.get(1) : null;
        if (miPushCommandMessage.getCommand().equals(MiPushClient.COMMAND_REGISTER)) {
            if (miPushCommandMessage.getResultCode() == ErrorCode.SUCCESS) {
                Constant.mRegId = cmdArg1 ;
            }
        }
    }

    /**
     * 根据返回的命令的类型相应的处理各个信息
     * @param context 程序运行的上下文
     * @param miPushCommandMessage 返回的消息，该消息封装在MiPushCommandMessage类中
     */
    @Override
    public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onCommandResult(context, miPushCommandMessage);
        long resultCode = miPushCommandMessage.getResultCode();
        List<String> arguments = miPushCommandMessage.getCommandArguments();
        String arg1 = (arguments != null && arguments.size() > 0) ? arguments.get(0) : null;
        String arg2 = (arguments != null && arguments.size() > 1) ? arguments.get(1) : null; // 第二个参数有可能没有
        if (resultCode == ErrorCode.SUCCESS) {

            if (MiPushClient.COMMAND_REGISTER.equals(miPushCommandMessage.getCommand())) {
                // 处理注册成功的返回码
                Constant.mRegId = arg1;
                MyUtils.showToast(context,"注册成功");
                Log.i(Constant.TAG,Constant.REGISTER_SUCCESS);

            } else if (MiPushClient.COMMAND_SET_ALIAS.equals(miPushCommandMessage.getCommand())) {
                // 处理设置别名的返回码
                Constant.mAlias = arg1;
                MyUtils.showToast(context,"设置别名成功");
                Log.i(Constant.TAG, Constant.SET_ALIAS_SUCCESS);

            } else if (MiPushClient.COMMAND_SET_ACCOUNT.equals(miPushCommandMessage.getCommand())) {
                // 处理设置账户的返回码
                Constant.mUserAccount = arg1;
                MyUtils.showToast(context,"设置账户成功");
                Log.i(Constant.TAG,Constant.SET_USER_ACCOUNT_SUCCESS);

            } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(miPushCommandMessage.getCommand())) {
                // 处理设置接收时间段的返回码
                Constant.mStartTime = arg1;
                Constant.mEndTime = arg2;
                MyUtils.showToast(context,"设置时间段成功");
                Log.i(Constant.TAG,Constant.SET_ACCEPT_TIME_SUCCESS);

            } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(miPushCommandMessage.getCommand())) {
                // 处理订阅频道的返回码
                Constant.mSubscribeTopic = arg1;
                MyUtils.showToast(context,"订阅主题成功");
                Log.i(Constant.TAG, Constant.SUBSCRIBE_TOPIC_SUCCESS);

            } else if (MiPushClient.COMMAND_UNSET_ACCOUNT.equals(miPushCommandMessage.getCommand())) {
                // 处理取消设置账户的返回码
                Constant.mUserAccount = arg1;
                MyUtils.showToast(context,"取消账户成功");
                Log.i(Constant.TAG, Constant.UNSET_ACCOUNT_SUCCESS);

            } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(miPushCommandMessage.getCommand())) {
                // 处理取消设置别名的返回码
                Constant.mAlias = arg1;
                MyUtils.showToast(context,"取消别名成功");
                Log.i(Constant.TAG, Constant.UNSET_ALIAS_SUCCESS);

            } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(miPushCommandMessage.getCommand())) {
                // 处理取消订阅频道的返回码
                Constant.mSubscribeTopic = arg1;
                MyUtils.showToast(context,"取消订阅成功");
                Log.i(Constant.TAG, Constant.UN_SUBSRCIBE_TOPIC_SUCCESS);
            }
        }
    }
}
