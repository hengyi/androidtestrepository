package cn.hylin.edu.szu.testxiaomipush.bean;

/**
 * Author：林恒宜 on 16-8-4 14:12
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyMessage {
    private String topic;
    private String content;

    public MyMessage(String topic, String content) {
        this.topic = topic;
        this.content = content;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTopic() {
        return topic;
    }

    public String getContent() {
        return content;
    }
}
