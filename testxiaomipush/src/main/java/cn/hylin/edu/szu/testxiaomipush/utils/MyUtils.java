package cn.hylin.edu.szu.testxiaomipush.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Author：林恒宜 on 16-8-4 13:51
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class MyUtils {
    public static void showToast (Context context, String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }
}
