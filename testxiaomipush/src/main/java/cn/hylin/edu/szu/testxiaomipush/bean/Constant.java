package cn.hylin.edu.szu.testxiaomipush.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Author：林恒宜 on 16-8-4 11:08
 * Email：hylin601@126.com
 * Github：https://github.com/hengyilin
 * Version：1.0
 * Description :
 */
public class Constant {
    public static final String TAG = "logcat_tag";

    public static final String APPID = "";
    public static final String APPKEY = "";

    public static String mRegId ;
    public static String mAlias ;
    public static String mUserAccount;
    public static String mSubscribeTopic;
    public static String mStartTime ;
    public static String mEndTime ;


    public static final String REGISTER_SUCCESS = "register push service success";
    public static final String SET_ALIAS_SUCCESS = "set alias success";
    public static final String SET_USER_ACCOUNT_SUCCESS = "set user account success";
    public static final String SUBSCRIBE_TOPIC_SUCCESS = "subscribe topic success";
    public static final String SET_ACCEPT_TIME_SUCCESS = "set accept time success";
    public static final String UNSET_ACCOUNT_SUCCESS = "unset account success";
    public static final String UNSET_ALIAS_SUCCESS = "unset alias success";
    public static final String UN_SUBSRCIBE_TOPIC_SUCCESS = "unsubscribe topic success";
    public static final String RECEIVE_THROUGH_MESSAGE_FROM_SERVER = "receive through message from server success";
    public static final String RECEIVE_MESSAGE_SUCCESS = "receive message success";
    public static final String RECEIVE_NOTIFICATION_MESSAGE_SUCCESS = "receive notification message success";
    public static final String NOTIFIVATION_MESSAGE_ONCLICK_SUCCESS = "notification message onclick success";


    public static List<MyMessage> msgList = new ArrayList<>();










}
